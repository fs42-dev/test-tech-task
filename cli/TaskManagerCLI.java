import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class TaskManagerCLI {
    private static final String BASE_URL = "http://localhost:8080/api/tasks";
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        HttpClient client = HttpClient.newHttpClient();

        while (true) {
            System.out.println("1. Ajouter une nouvelle tâche");
            System.out.println("2. Afficher les tâches");
            System.out.println("3. Marquer une tâche comme complétée");
            System.out.println("4. Supprimer une tâche");
            System.out.println("5. Quitter");
            System.out.print("Choisissez une option: ");
            int choice = scanner.nextInt();
            scanner.nextLine();  // Consume newline

            try {
                switch (choice) {
                    case 1 -> {
                        System.out.print("Titre: ");
                        String title = scanner.nextLine();
                        if (title.length() < 3 || title.length() > 20) {
                            System.out.println("Le titre doit comporter entre 3 et 20 caractères.");
                            break;
                        }

                        System.out.print("Description: ");
                        String description = scanner.nextLine();
                        if (description.length() < 5 || description.length() > 100) {
                            System.out.println("La description doit comporter entre 5 et 100 caractères.");
                            break;
                        }

                        System.out.print("Date d'échéance (YYYY-MM-DD): ");
                        String dueDateString = scanner.nextLine();
                        LocalDate dueDate;
                        try {
                            dueDate = LocalDate.parse(dueDateString, DATE_FORMATTER);
                        } catch (DateTimeParseException e) {
                            System.out.println("Le format de la date est incorrect. Veuillez utiliser le format YYYY-MM-DD.");
                            break;
                        }

                        if (dueDate.isBefore(LocalDate.now())) {
                            System.out.println("La date d'échéance ne peut pas être antérieure à aujourd'hui.");
                            break;
                        }

                        String newTaskJson = String.format("{\"title\":\"%s\",\"description\":\"%s\",\"dueDate\":\"%s\"}", title, description, dueDate);
                        HttpRequest request = HttpRequest.newBuilder()
                                .uri(URI.create(BASE_URL))
                                .header("Content-Type", "application/json")
                                .POST(HttpRequest.BodyPublishers.ofString(newTaskJson))
                                .build();
                        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
                        System.out.println("Tâche ajoutée: " + response.body());
                    }
                    case 2 -> {
                        HttpRequest getRequest = HttpRequest.newBuilder()
                                .uri(URI.create(BASE_URL))
                                .build();
                        HttpResponse<String> getResponse = client.send(getRequest, HttpResponse.BodyHandlers.ofString());
                        System.out.println("Tâches: " + getResponse.body());
                    }
                    case 3 -> {
                        System.out.print("ID de la tâche à marquer comme complétée: ");
                        long id = scanner.nextLong();
                        HttpRequest completeRequest = HttpRequest.newBuilder()
                                .uri(URI.create(BASE_URL + "/" + id + "/complete"))
                                .header("Content-Type", "application/json")
                                .PUT(HttpRequest.BodyPublishers.noBody())
                                .build();
                        HttpResponse<String> completeResponse = client.send(completeRequest, HttpResponse.BodyHandlers.ofString());
                        if (completeResponse.statusCode() == 404) {
                            System.out.println("La tâche avec l'ID " + id + " n'existe pas.");
                        } else {
                            System.out.println("Tâche complétée: " + completeResponse.body());
                        }
                    }
                    case 4 -> {
                        System.out.print("ID de la tâche à supprimer: ");
                        long deleteId = scanner.nextLong();
                        HttpRequest deleteRequest = HttpRequest.newBuilder()
                                .uri(URI.create(BASE_URL + "/" + deleteId))
                                .DELETE()
                                .build();
                        HttpResponse<String> deleteResponse = client.send(deleteRequest, HttpResponse.BodyHandlers.ofString());
                        System.out.println("Tâche supprimée.");
                    }
                    case 5 -> {
                        System.out.println("Au revoir!");
                        System.exit(0);
                    }
                    default -> System.out.println("Option invalide. Veuillez réessayer.");
                }
            } catch (Exception e) {
                System.err.println("Erreur: " + e.getMessage());
            }
        }
    }
}

package com.task.service;

import com.task.model.Task;
import com.task.repository.TaskRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for managing tasks. This class provides methods to perform CRUD operations on
 * tasks.
 */
@Service
public class TaskService {

  @Autowired private TaskRepository taskRepository;

  /**
   * Retrieves all tasks from the repository.
   *
   * @return a list of all tasks
   */
  public List<Task> getAllTasks() {
    return taskRepository.findAll();
  }

  /**
   * Retrieves a task by its ID.
   *
   * @param id the ID of the task
   * @return an Optional containing the task if found, or empty if not found
   */
  public Optional<Task> getTaskById(Long id) {
    return taskRepository.findById(id);
  }

  /**
   * Creates a new task in the repository.
   *
   * @param task the task to be created
   * @return the created task
   */
  public Task createTask(Task task) {
    return taskRepository.save(task);
  }

  /**
   * Mark a task as completed.
   *
   * @param id the ID of the task
   * @return The task marked as completed
   */
  public Task markTaskAsCompleted(Long id) {
    Optional<Task> taskOpt = taskRepository.findById(id);
    if (taskOpt.isPresent()) {
      Task task = taskOpt.get();
      task.setCompleted(true);
      return taskRepository.save(task);
    }
    return null;
  }

  /**
   * Delete a task from the repository.
   *
   * @param id the ID of the task
   */
  public void deleteTask(Long id) {
    taskRepository.deleteById(id);
  }
}

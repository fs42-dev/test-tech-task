package com.task.controller;

import com.task.model.Task;
import com.task.service.TaskService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * This class serves as a REST controller for managing tasks. It maps incoming HTTP requests
 *
 * <p>to appropriate methods for CRUD operations (Create, Read, Update, Delete) on tasks.
 */
@RestController
@RequestMapping("/api/tasks")
public class TaskController {
  @Autowired private TaskService taskService;

  /**
   * Retrieves all tasks from the system using the injected TaskService and returns them as a list.
   *
   * @return A list of all Task objects present in the system.
   * @throws Exception If an error occurs while retrieving tasks from the TaskService.
   */
  @GetMapping
  public List<Task> getAllTasks() {
    return taskService.getAllTasks();
  }

  /**
   * Retrieves a specific task by its ID using the injected TaskService.
   *
   * @param id The unique identifier of the task to be retrieved.
   * @return A ResponseEntity containing either the retrieved Task object wrapped in a 200 (OK)
   *     response or a 404 (Not Found) response if the task is not found.
   * @throws Exception If an error occurs while retrieving the task from the TaskService.
   */
  @GetMapping("/{id}")
  public ResponseEntity<Task> getTaskById(@PathVariable Long id) {
    return taskService
        .getTaskById(id)
        .map(ResponseEntity::ok)
        .orElse(ResponseEntity.notFound().build());
  }

  /**
   * Creates a new task using the provided Task object in the request body. Delegates the creation
   * to the injected TaskService and returns the created task.
   *
   * @param task The Task object representing the new task to be created.
   * @return The newly created Task object.
   */
  @PostMapping
  public Task createTask(@RequestBody Task task) {
    return taskService.createTask(task);
  }

  /**
   * Deletes a task identified by its ID using the injected TaskService. Returns a 204 (No Content)
   * response if successful.
   *
   * @param id The unique identifier of the task to be deleted.
   */
  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteTask(@PathVariable Long id) {
    taskService.deleteTask(id);
    return ResponseEntity.noContent().build();
  }

  /**
   * Marks a task identified by its ID as completed using the injected TaskService. Returns a 200
   * (OK) response with the updated Task object if successful, or a 404 (Not Found) response if the
   * task is not found.
   *
   * @param id The unique identifier of the task to be marked as completed.
   * @return A ResponseEntity containing either the updated Task object or a 404 (Not Found)
   *     response.
   */
  @PutMapping("/{id}/complete")
  public ResponseEntity<Task> markTaskAsCompleted(@PathVariable Long id) {
    Task updatedTask = taskService.markTaskAsCompleted(id);
    if (updatedTask != null) {
      return ResponseEntity.ok(updatedTask);
    }
    return ResponseEntity.notFound().build();
  }
}

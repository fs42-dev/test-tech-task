package com.task.controller;

import com.task.model.Task;
import com.task.service.TaskService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TaskControllerTest {

    @Mock
    private TaskService taskService;

    @InjectMocks
    private TaskController taskController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetAllTasks() {
        Task task1 = new Task(1L, "Task 1", "Description 1", "2024-07-01", false);
        Task task2 = new Task(2L, "Task 2", "Description 2", "2024-07-02", false);

        when(taskService.getAllTasks()).thenReturn(Arrays.asList(task1, task2));

        List<Task> tasks = taskController.getAllTasks();

        assertNotNull(tasks);
        assertEquals(2, tasks.size());
        verify(taskService, times(1)).getAllTasks();
    }

    @Test
    public void testGetTaskById() {
        Task task = new Task(1L, "Task 1", "Description 1", "2024-07-01", false);
        when(taskService.getTaskById(1L)).thenReturn(Optional.of(task));

        ResponseEntity<Task> response = taskController.getTaskById(1L);

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("Task 1", response.getBody().getTitle());
        verify(taskService, times(1)).getTaskById(1L);
    }

    @Test
    public void testGetTaskByIdNotFound() {
        when(taskService.getTaskById(1L)).thenReturn(Optional.empty());

        ResponseEntity<Task> response = taskController.getTaskById(1L);

        assertNotNull(response);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        verify(taskService, times(1)).getTaskById(1L);
    }

    @Test
    public void testCreateTask() {
        Task task = new Task(1L, "Task 1", "Description 1", "2024-07-01", false);
        when(taskService.createTask(any(Task.class))).thenReturn(task);

        Task createdTask = taskController.createTask(task);

        assertNotNull(createdTask);
        assertEquals("Task 1", createdTask.getTitle());
        verify(taskService, times(1)).createTask(any(Task.class));
    }

    @Test
    public void testDeleteTask() {
        doNothing().when(taskService).deleteTask(1L);

        ResponseEntity<Void> response = taskController.deleteTask(1L);

        assertNotNull(response);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(taskService, times(1)).deleteTask(1L);
    }

    @Test
    public void testMarkTaskAsCompleted() {
        Task task = new Task(1L, "Task 1", "Description 1", "2024-07-01", false);
        when(taskService.markTaskAsCompleted(1L)).thenAnswer(invocation -> {
            task.setCompleted(true);
            return task;
        });

        ResponseEntity<Task> response = taskController.markTaskAsCompleted(1L);

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(true, response.getBody().isCompleted());
        verify(taskService, times(1)).markTaskAsCompleted(1L);
    }

    @Test
    public void testMarkTaskAsCompletedNotFound() {
        when(taskService.markTaskAsCompleted(1L)).thenReturn(null);

        ResponseEntity<Task> response = taskController.markTaskAsCompleted(1L);

        assertNotNull(response);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        verify(taskService, times(1)).markTaskAsCompleted(1L);
    }
}
